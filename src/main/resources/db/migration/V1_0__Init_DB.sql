create table product_price(
    id bigserial primary key,
    product_id bigint not null,
    price numeric(11, 2) not null
);

create unique index product_price_product_id_idx on product_price(product_id);