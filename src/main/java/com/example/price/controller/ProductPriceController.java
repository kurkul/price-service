package com.example.price.controller;

import com.example.price.service.ProductPriceService;
import com.example.warehouse.shared.dto.ProductPriceDto;
import com.example.warehouse.shared.dto.ResponseDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.tags.Tags;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/products/prices")
@Tags(@Tag(name = "Product prices", description = "Access to prices of products of a warehouse"))
@RequiredArgsConstructor
public class ProductPriceController {

    private final ProductPriceService productPriceService;

    @GetMapping("/{productId}")
    @Operation(summary = "Get product price by product id")
    public ResponseDto<ProductPriceDto> getPrice(@PathVariable long productId) {
        return ResponseDto.success(
                        productPriceService.getPriceByProductId(productId));
    }

    @GetMapping
    @Operation(summary = "Get multiple products prices by ids")
    public ResponseDto<List<ProductPriceDto>> getPrices(@RequestParam(name = "product_ids") List<Long> productIds) {
        return ResponseDto.success(
                productPriceService.getPricesByProductIds(productIds));
    }

}
