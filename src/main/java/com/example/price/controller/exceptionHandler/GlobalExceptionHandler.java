package com.example.price.controller.exceptionHandler;

import com.example.warehouse.shared.dto.ResponseDto;
import com.example.warehouse.shared.exception.EntityNotFoundException;
import com.example.warehouse.shared.exception.ValidationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseDto<?> handleEntityNotFound(EntityNotFoundException e) {
        return ResponseDto.failure(e.getMessage());
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseDto<?> handleInvalidParameter(MethodArgumentTypeMismatchException e) {
        String error = "Value of param '%s' is invalid";

        return ResponseDto.failure(String.format(error, e.getName()));
    }

    @ExceptionHandler(ValidationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseDto<?> handleValidationException(ValidationException e) {
        return ResponseDto.failure(e.getMessage());
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    public ResponseDto<?> handleUnprocessableEntity(HttpMessageNotReadableException e) {
        return ResponseDto.failure("Cannot deserialize request body");
    }

}
