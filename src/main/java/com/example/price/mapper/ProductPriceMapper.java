package com.example.price.mapper;

import com.example.price.entity.ProductPrice;
import com.example.warehouse.shared.dto.ProductPriceDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ProductPriceMapper {

    ProductPriceDto convert(ProductPrice productPrice);

    ProductPrice convert(ProductPriceDto dto);

}
