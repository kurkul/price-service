package com.example.price.service;

import com.example.warehouse.shared.dto.ProductPriceDto;

import java.util.List;

public interface ProductPriceService {

    ProductPriceDto getPriceByProductId(long productId);
    List<ProductPriceDto> getPricesByProductIds(List<Long> productId);

}
