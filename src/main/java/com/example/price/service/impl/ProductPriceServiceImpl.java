package com.example.price.service.impl;

import com.example.price.mapper.ProductPriceMapper;
import com.example.price.repository.ProductPriceRepository;
import com.example.price.service.ProductPriceService;
import com.example.warehouse.shared.dto.ProductPriceDto;
import com.example.warehouse.shared.exception.CommonException;
import com.example.warehouse.shared.exception.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProductPriceServiceImpl implements ProductPriceService {

    private final ProductPriceRepository productPriceRepository;
    private final ProductPriceMapper productPriceMapper;

    @Override
    public ProductPriceDto getPriceByProductId(long productId) {
        log.debug("getPriceByProductId: Starting with: productId:{}", productId);
        try {
            ProductPriceDto productPrice = productPriceRepository
                    .findByProductId(productId)
                    .map(productPriceMapper::convert)
                    .orElseThrow(() -> new EntityNotFoundException("Can't find price by productId " + productId));

            log.info("getPriceByProductId: Finished: {}", productId);
            return productPrice;
        } catch (EntityNotFoundException e) {
            log.error("getPriceByProductId: Failed with: {}", productId, e);
            throw e;
        } catch (Exception e) {
            log.error("getPriceByProductId: Failed with: {}", productId, e);
            throw new CommonException("Failed to ged price by product id");
        }
    }

    @Override
    public List<ProductPriceDto> getPricesByProductIds(List<Long> productIds) {
        log.debug("getPricesByProductIds: Starting with: productIds:{}", productIds);
        try {
            List<ProductPriceDto> prices = productIds.stream()
                    .map(this::getPriceByProductId)
                    .collect(Collectors.toList());

            log.info("getPricesByProductIds: Finished: {}", productIds);
            return prices;
        } catch (EntityNotFoundException | CommonException e) {
            log.error("getPricesByProductIds: Failed with: {}", productIds, e);
            throw e;
        } catch (Exception e) {
            log.error("getPricesByProductIds: Failed with: {}", productIds, e);
            throw new CommonException("Failed to get prices by product ids");
        }
    }

}
