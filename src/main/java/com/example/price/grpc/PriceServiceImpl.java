package com.example.price.grpc;

import com.example.price.service.ProductPriceService;
import com.example.warehouse.shared.dto.ProductPriceDto;
import io.grpc.stub.StreamObserver;
import lombok.RequiredArgsConstructor;
import net.devh.boot.grpc.server.service.GrpcService;

@GrpcService
@RequiredArgsConstructor
public class PriceServiceImpl extends PriceServiceGrpc.PriceServiceImplBase {

    private final ProductPriceService productPriceService;

    @Override
    public void getPrice(PriceRequest request,
                         StreamObserver<PriceResponse> responseObserver) {

        ProductPriceDto price = productPriceService.getPriceByProductId(request.getProductId());
        PriceResponse response = PriceResponse.newBuilder()
                .setProductId(price.getProductId())
                .setPrice(price.getPrice().doubleValue())
                .build();

        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

}
