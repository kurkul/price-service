package com.example.price.grpc;

import com.example.price.repository.ProductPriceRepository;
import com.google.protobuf.Empty;
import io.grpc.stub.StreamObserver;
import lombok.RequiredArgsConstructor;
import net.devh.boot.grpc.server.service.GrpcService;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.PeriodicTrigger;

import java.util.concurrent.TimeUnit;

@GrpcService
@RequiredArgsConstructor
public class PriceStreamingServiceImpl extends PriceStreamingServiceGrpc.PriceStreamingServiceImplBase {

    private final ProductPriceRepository priceRepository;
    private final ThreadPoolTaskScheduler priceScheduler;

    @Override
    public void providePrices(Empty request, StreamObserver<PriceResponse> responseObserver) {
        Runnable runnable = () ->
                priceRepository.findAll().forEach(price ->
                        responseObserver.onNext(PriceResponse.newBuilder()
                                .setProductId(price.getProductId())
                                .setPrice(price.getPrice().doubleValue())
                                .build()));

        PeriodicTrigger periodicTrigger = new PeriodicTrigger(10, TimeUnit.SECONDS);
        periodicTrigger.setFixedRate(true);
        priceScheduler.schedule(runnable, periodicTrigger);
    }

}
