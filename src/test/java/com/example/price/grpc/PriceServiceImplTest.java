package com.example.price.grpc;

import com.example.price.initializer.PostgresInitializer;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test")
@ContextConfiguration(initializers = { PostgresInitializer.class })
public class PriceServiceImplTest {

    @GrpcClient("inProcess")
    private PriceServiceGrpc.PriceServiceBlockingStub priceService;

    @Test
    void when_getPriceOfExistingProduct_expect_priceReceived() {
        PriceRequest request = getPriceRequest(1);

        PriceResponse response = priceService.getPrice(request);
        assertNotNull(response);
        assertEquals(100.00, response.getPrice());
    }

    @Test
    void when_priceNotFound_expect_givenExceptionIsThrown() {
        PriceRequest request = getPriceRequest(100);

        StatusRuntimeException e = assertThrows(StatusRuntimeException.class,
                () -> priceService.getPrice(request));
        assertEquals(Status.NOT_FOUND.getCode(), e.getStatus().getCode());
        assertEquals("Can't find price by productId 100", e.getStatus().getDescription());
    }

    private PriceRequest getPriceRequest(long productId) {
        return PriceRequest.newBuilder().setProductId(productId).build();
    }

}
