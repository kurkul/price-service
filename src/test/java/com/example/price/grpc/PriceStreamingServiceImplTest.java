package com.example.price.grpc;

import com.example.price.entity.ProductPrice;
import com.example.price.initializer.PostgresInitializer;
import com.example.price.repository.ProductPriceRepository;
import com.google.protobuf.Empty;
import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.awaitility.Duration;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import static org.awaitility.Awaitility.await;

@SpringBootTest
@ActiveProfiles("test")
@ContextConfiguration(initializers = { PostgresInitializer.class })
public class PriceStreamingServiceImplTest {

    @Autowired
    private ProductPriceRepository priceRepository;

    @GrpcClient("inProcess")
    private PriceStreamingServiceGrpc.PriceStreamingServiceStub priceService;

    @Test
    @DirtiesContext
    void providePrices() {
        List<PriceResponse> result = new ArrayList<>();
        StreamObserver<PriceResponse> responseObserver =
                new StreamObserver<>() {
                    @Override
                    public void onNext(PriceResponse value) {
                        result.add(value);
                    }

                    @Override
                    public void onError(Throwable t) {
                        fail("Failed providePrices test", t);
                    }

                    @Override
                    public void onCompleted() {
                    }
                };

        priceService.providePrices(Empty.newBuilder().build(), responseObserver);

        await().atMost(Duration.TEN_SECONDS).untilAsserted(() ->
                priceRepository.findAll().forEach(price ->
                        assertThat(result).contains(buildPriceResponse(price)))
        );
    }

    private PriceResponse buildPriceResponse(ProductPrice price) {
        return PriceResponse.newBuilder()
                .setProductId(price.getProductId())
                .setPrice(price.getPrice().doubleValue())
                .build();
    }

}
