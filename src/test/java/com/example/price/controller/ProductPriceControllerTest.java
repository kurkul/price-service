package com.example.price.controller;

import com.example.price.initializer.PostgresInitializer;
import com.example.warehouse.shared.dto.ProductPriceDto;
import com.example.warehouse.shared.dto.ResponseDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = {PostgresInitializer.class})
public class ProductPriceControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void when_getPriceByProductId_expect_status200AndPriceReceived() {
        ResponseEntity<ResponseDto<ProductPriceDto>> response =
                restTemplate.exchange("/products/prices/1", HttpMethod.GET,
                        null, new ParameterizedTypeReference<>() {});

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(new BigDecimal("100.00"), response.getBody().getData().getPrice());
    }

    @Test
    void when_getPricesByProductIds_expect_status200AndPricesListReceived() {
        ResponseEntity<ResponseDto<List<ProductPriceDto>>> response =
                restTemplate.exchange("/products/prices?product_ids=1,2", HttpMethod.GET,
                        null, new ParameterizedTypeReference<>() {});

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertThat(response.getBody().getData()).containsExactlyInAnyOrder(
                buildProductPrice(1, 1, new BigDecimal("100.00")),
                buildProductPrice(2, 2, new BigDecimal("150.00")));
    }

    @ParameterizedTest
    @MethodSource
    void when_wrongParamsPassed_expect_givenStatusAndErrorMessageReceived(String url,
                                                                          HttpStatus expectedStatus,
                                                                          String expectedError) {
        ResponseEntity<ResponseDto<Object>> response =
                restTemplate.exchange(url, HttpMethod.GET,
                        null, new ParameterizedTypeReference<>() {});

        assertExchangeFailure(response, expectedStatus, expectedError);
    }

    private static Stream<Arguments> when_wrongParamsPassed_expect_givenStatusAndErrorMessageReceived() {
        return Stream.of(
                Arguments.of("/products/prices/1000",
                        HttpStatus.NOT_FOUND,
                        "Can't find price by productId 1000"),
                Arguments.of("/products/prices/string",
                        HttpStatus.BAD_REQUEST,
                        "Value of param 'productId' is invalid"),
                Arguments.of("/products/prices?product_ids=1,1000",
                        HttpStatus.NOT_FOUND,
                        "Can't find price by productId 1000"),
                Arguments.of("/products/prices?product_ids=1&product_ids=string",
                        HttpStatus.BAD_REQUEST,
                        "Value of param 'product_ids' is invalid")
        );
    }

    protected <T> void assertExchangeFailure(ResponseEntity<ResponseDto<T>> responseEntity,
                                             HttpStatus expectedStatus,
                                             String expectedError) {

        ResponseDto<?> responseDto = responseEntity.getBody();

        assertEquals(expectedStatus, responseEntity.getStatusCode());
        assertNotNull(responseDto);
        assertEquals(expectedError, responseDto.getError());
        assertNull(responseDto.getData());
    }

    private ProductPriceDto buildProductPrice(long id, long productId, BigDecimal price) {
        return ProductPriceDto.builder()
                .id(id)
                .productId(productId)
                .price(price).build();
    }

}
